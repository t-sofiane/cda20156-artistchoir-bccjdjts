package fr.afpa.cda20156_artistchoir;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import fr.afpa.cda20156_artistchoir.controller.ConnectionController;
import fr.afpa.cda20156_artistchoir.controller.RegistrationController;

public class MainActivity extends AppCompatActivity {

    private Button connection;
    private Button createAccount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Intent intent = getIntent();
        String loginText = intent.getStringExtra("loginSuccess");
        if (loginText != null) {
            Toast.makeText(MainActivity.this, loginText, Toast.LENGTH_LONG).show();
        }

        this.createAccount = findViewById(R.id.id_btn_createAccount);
        this.connection = findViewById(R.id.id_btn_connection);

        this.createAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent().setClass(MainActivity.this, RegistrationController.class));
            }
        });

        this.connection.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent().setClass(MainActivity.this, ConnectionController.class));
            }
        });
    }
}