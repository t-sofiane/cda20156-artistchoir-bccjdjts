package fr.afpa.cda20156_artistchoir.business;

import java.util.Date;
import java.util.Optional;

import fr.afpa.cda20156_artistchoir.beans.Photo;
import fr.afpa.cda20156_artistchoir.beans.Product;
import fr.afpa.cda20156_artistchoir.beans.User;
import fr.afpa.cda20156_artistchoir.controller.CreateProductController;
import io.realm.Realm;
import io.realm.RealmResults;

public class ProductBusiness {

    private Realm realm;
    private User userRealm;
    private CreateProductController createProductController;
    private RealmResults results;

    public ProductBusiness(){
        this.realm = Realm.getDefaultInstance();
    }

    /**
     * @author jerome - Méthode de save du produit
     * @author Julien
     * @author sofiane
     *
     * @param creaDate date de création du produit
     * @param productName nom du produit
     * @param productBrand marque du produit
     * @param productDescription description du produit
     * @param productRef référence du produit
     * @param user Utilisateur récupéré via la session ( Classe SessionManager )
     */
    public void saveProduct(Date creaDate, String productName, String productBrand, String productDescription, String productRef, User user, String photoPath){

        this.realm.executeTransactionAsync(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                Number highestProductId = realm
                        .where(Product.class)
                        .max("id");
                int newIdProduct = (highestProductId == null) ? 1 : highestProductId.intValue() + 1;
                Product product = realm
                        .createObject(Product.class, newIdProduct);

                if (photoPath != null){
                    Number highestPhotoId = realm
                            .where(Photo.class)
                            .max("id");
                    int newIdPhoto = (highestPhotoId == null) ? 1 : highestPhotoId.intValue() + 1;
                    Photo photo = realm
                            .createObject(Photo.class, newIdPhoto);
                    photo.setLink(photoPath);
                    photo.setActive(true);
                    product.setPhoto(photo);
                }

                userRealm = realm
                        .where(User.class)
                        .equalTo("id", user.getId())
                        .findFirst();

                product.setName(productName);
                product.setBrand(productBrand);
                product.setDescription(productDescription);

                product.setReference(productRef);
                product.setCreationDate(creaDate);
                product.setAuthor(userRealm);
                // dès lors que la photo est fonctionnelle (paramètre) ->  product.setPhoto();
            }
        }, new Realm.Transaction.OnSuccess() {
            @Override
            public void onSuccess() {
            }
        }, new Realm.Transaction.OnError() {
            @Override
            public void onError(Throwable error) {
            }
        });
    }

    /**
     * @author : jerome
     *
     * Si Realm trouve une référence similaire que celle passée en paramètre, cette méthode retourne un Optional non null à traiter dans le controller de product
     * @param reference référence du produit saisi par le user pour un create
     */
    public Optional<Product> retrieveProductByReference(String reference){
        Optional<Product> productRef = Optional.ofNullable(realm.where(Product.class).equalTo("reference", reference).findFirst());
        return productRef;
    }

    /**
     * @author jerome
     *
     * Cette méthode permet de récupérer tous les produits enregistrés depuis la base de données.
     * @return Retourne une liste de RealmResults à destination du RecyclerView
     */
    public RealmResults<Product> retrieveAllProductsFromDb() {
        Optional<RealmResults<Product>> listProductRealm = Optional.ofNullable(realm
                .where(Product.class)
                .findAll());
            if (listProductRealm.isPresent()){
                results = listProductRealm.get();
            }
        return results;
    }

    /**
     * @author jerome
     *
     * Méthode permettant la suppression du produit grêce à l'id récupéré depuis l'item view construit dans la classe ViewHolder.
     * Cette méthode est appelée dans la classe ProductListAdapter au clic de l'icône delete
     * @param idProduct id du produit récupéré
     */
    public void deleteProductById(Long idProduct){
        Product productToDelete = realm.where(Product.class).equalTo("id", idProduct).findFirst();
        realm.beginTransaction();
        productToDelete.deleteFromRealm();
        realm.commitTransaction();
    }

    /**
     * @author T sofiane
     * méthode de récupération d'un produit par id
     * @param userId id du produit
     */
    public Product getProductById(Long userId){
        return this.realm.where(Product.class).equalTo("id", userId)
                .findFirst();
    }
    /**
     * @author T sofiane
     * méthode de mettre à jour d'un produit
     * @param productName nom du produit
     * @param productBrand marque du produit
     * @param productDescription description du produit
     * @param productRef référence du produit
     */
    public void updateProduct(long idProduct, String productName, String productBrand, String productDescription, String productRef) {
        Product product=this.getProductById(idProduct);
        realm.beginTransaction();
        product.setName(productName);
        product.setBrand(productBrand);
        product.setDescription(productDescription);
        product.setReference(productRef);
        realm.commitTransaction();
    }
}