package fr.afpa.cda20156_artistchoir.business;

import fr.afpa.cda20156_artistchoir.beans.Login;
import io.realm.Realm;

public class UserDashboardBusiness {

    private Realm realm;

    public UserDashboardBusiness() {
        this.realm = Realm.getDefaultInstance();
    }

    public Login getLoginByUserId(Long userId){
        return this.realm.where(Login.class).equalTo("id", userId)
                .findFirst();
    }
}
