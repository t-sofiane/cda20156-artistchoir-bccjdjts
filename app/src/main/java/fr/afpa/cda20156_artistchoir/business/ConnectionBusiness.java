package fr.afpa.cda20156_artistchoir.business;

import fr.afpa.cda20156_artistchoir.beans.Login;
import io.realm.Realm;

/**
 * Couche métier de la connexion
 */
public class ConnectionBusiness {

    private Realm realm;

    public ConnectionBusiness() {
        this.realm = Realm.getDefaultInstance();
    }

    /**
     * Récupère un login grâce au pseudonyme et mot de passe.
     *
     * @param username : le pseudonyme servant à récupérer le login.
     * @param password : le mot de passe servant à récupérer le login
     * @return Login : le login récupéré dans la base dee données.
     */
    public Login get(String username, String password) {
        return this.realm.where(Login.class)
                .equalTo("username", username)
                .equalTo("password", password)
                .findFirst();
    }


}
