package fr.afpa.cda20156_artistchoir.business;

import fr.afpa.cda20156_artistchoir.beans.Login;
import fr.afpa.cda20156_artistchoir.beans.User;
import io.realm.Realm;

/**
 * Couche métier de l'enregistrement utilsateur
 */
public class RegistrationBusiness {

    private Realm realm;

    public RegistrationBusiness() {
        this.realm = Realm.getDefaultInstance();
    }


    public void saveData(User userToRegister, Login loginToRegister) {
        this.realm.executeTransactionAsync(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                Number maxIdUser = realm.where(User.class).max("id");
                int newKeyUser = (maxIdUser == null) ? 1 : maxIdUser.intValue() + 1;

                Number maxIdLogin = realm.where(Login.class).max("id");
                int newKeyLogin = (maxIdLogin == null) ? 1 : maxIdLogin.intValue() + 1;

                User user = realm.createObject(User.class, newKeyUser);
                Login login = realm.createObject(Login.class, newKeyLogin);

                login.setUsername(loginToRegister.getUsername());
                login.setPassword(loginToRegister.getPassword());

                user.setEmail(userToRegister.getEmail());
                user.setActive(true);
                user.setLogin(login);
                login.setUser(user);
            }
        }, new Realm.Transaction.OnSuccess() {
            @Override
            public void onSuccess() {
            }
        }, new Realm.Transaction.OnError() {
            @Override
            public void onError(Throwable error) {
            }
        });
    }

    /**
     * Récupère un utilisateur d'après son email
     *
     * @param email : l'email servant à récupérer l'utilisateur
     * @return User : retourne un utilisateur de la base de données
     */
    public User getUserByEmail(String email) {
        return this.realm.where(User.class).equalTo("email", email)
                .findFirst();
    }

    /**
     * Récupère un login d'après son pseudonyme
     *
     * @param username : le pseudonyme servant à récupérer l'utilisateur
     * @return Login : retourne un login de la base de données
     */
    public Login getLoginByUsername(String username) {
        return this.realm.where(Login.class).equalTo("username", username)
                .findFirst();
    }

    /**
     * @author T sofiane
     * Récupère un User d'après son Id
     *
     * @param idUser : l'id de l'utilisateur à récupérer
     * @return User : retourne un utilisateur à partir de la base de données
     */
    public User getUserById(Long idUser) {
        return this.realm.where(User.class).equalTo("id", idUser)
                .findFirst();
    }

    /**
     * @author T sofiane
     * Récupère un Login d'après son Id
     * @param idLogin
     * @return Logi : retourne un Login à partir de la base de données
     */
    public Login getLoginById(Long idLogin) {
        return this.realm.where(Login.class).equalTo("id", idLogin)
                .findFirst();
    }
    /**
     * @author T sofiane
     * méthode de mettre à jour d'un utilisateur
     * @param userToUpdate
     * @param loginToUpdate
     */
    public void updateData(User userToUpdate, Login loginToUpdate){
        User user=this.getUserById(userToUpdate.getId());
        Login login= this.getLoginById(loginToUpdate.getId());
        realm.beginTransaction();
        user.setEmail(userToUpdate.getEmail());
        login.setUsername(loginToUpdate.getUsername());
        login.setPassword(loginToUpdate.getPassword());
        realm.commitTransaction();
    }

}