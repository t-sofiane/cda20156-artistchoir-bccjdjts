package fr.afpa.cda20156_artistchoir;

import android.app.Application;

import io.realm.Realm;
import io.realm.RealmConfiguration;


/**
 * @author sofiane
 * la classe MyApplication qui hérite de la classe Application
 * et l'initialisation de Realm
 */
public class MyApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        Realm.init(this);
        RealmConfiguration configuration = new RealmConfiguration.Builder().name("RealmData.realm").build();
        Realm.setDefaultConfiguration(configuration);
    }
}