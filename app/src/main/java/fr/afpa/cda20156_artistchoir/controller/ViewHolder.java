package fr.afpa.cda20156_artistchoir.controller;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import fr.afpa.cda20156_artistchoir.R;

/**
 * @author jerome
 *
 * Création d'un view holder pour ce layout qui sera lié à l'adapter
 * On retrouve dans la méthode ViewHolder les éléments qui composent un item
 */
public class ViewHolder extends RecyclerView.ViewHolder {

    TextView productName, productReference;
    ImageView editIcon, deleteIcon, productImage;
    private CardView cardView;


    /**
     * @author jerome
     *
     * Méthode qui permet d'affecter à une View les éléments qui la composent dont :
     * @param itemView vue d'un item, composé des éléments : nom du produit, icone de
     * modification et icone de suppression
     */
    public ViewHolder(View itemView) {
        super(itemView);
        productImage = itemView.findViewById(R.id.product_image);
        productReference = itemView.findViewById(R.id.product_reference);
        productName = itemView.findViewById(R.id.product_name);
        editIcon = itemView.findViewById(R.id.edit_item);
        deleteIcon = itemView.findViewById(R.id.delete_item);
        cardView = itemView.findViewById(R.id.data_item_card);
    }
}
