package fr.afpa.cda20156_artistchoir.controller;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import fr.afpa.cda20156_artistchoir.MainActivity;
import fr.afpa.cda20156_artistchoir.R;
import fr.afpa.cda20156_artistchoir.beans.Login;
import fr.afpa.cda20156_artistchoir.beans.User;
import fr.afpa.cda20156_artistchoir.business.ConnectionBusiness;
import fr.afpa.cda20156_artistchoir.session.SessionManager;
import io.realm.Realm;

/**
 * Contrôleur de la connexion
 */
public class ConnectionController extends AppCompatActivity {

    private Realm realm;
    private Login login;
    private EditText inputUsername;
    private EditText inputPassword;
    private Button connect;
    private Button goBack;
    private ConnectionBusiness connectionBusiness;
    private SessionManager sessionManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_connection_controller);

        this.connectionBusiness = new ConnectionBusiness();
        Intent intent = getIntent();
        String registerText = intent.getStringExtra("registerSuccess");
        if (registerText != null) {
            Toast.makeText(ConnectionController.this, registerText, Toast.LENGTH_LONG).show();
        }

        Realm.init(getApplicationContext());
        this.realm = Realm.getDefaultInstance();

        this.inputUsername = findViewById(R.id.id_input_username);
        this.inputPassword = findViewById(R.id.id_input_password);
        this.connect = findViewById(R.id.id_btn_login);
        this.goBack = findViewById(R.id.id_btn_back);

        sessionManager = new SessionManager(this);

        this.goBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent().setClass(ConnectionController.this, MainActivity.class));
            }
        });

        this.connect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                login = connectionBusiness.get(inputUsername.getText().toString(), inputPassword.getText().toString());
                if (login != null) {
                    User user = login.getUser();
                    Intent intent = new Intent(ConnectionController.this, UserDashboardController.class);
                    sessionManager.createSession(user.getId());
                    intent.putExtra("loginSuccess", "Connexion réussie.");
                    startActivity(intent);
                } else {
                    Toast.makeText(ConnectionController.this, "Erreur de connexion, veuillez rééssayer.", Toast.LENGTH_LONG).show();
                }
            }
        });

    }

}