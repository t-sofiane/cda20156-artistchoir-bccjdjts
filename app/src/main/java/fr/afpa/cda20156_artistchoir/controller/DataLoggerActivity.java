package fr.afpa.cda20156_artistchoir.controller;

import android.app.Activity;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import fr.afpa.cda20156_artistchoir.R;
import fr.afpa.cda20156_artistchoir.beans.Product;
import fr.afpa.cda20156_artistchoir.business.ProductBusiness;
import fr.afpa.cda20156_artistchoir.session.SessionManager;
import io.realm.Realm;
import io.realm.RealmResults;

/**
 * @author jerome
 *
 * Classe qui construit l'activity activity_list_item_container, récupère la liste des produits créés
 */
public class DataLoggerActivity extends Activity {

    private ProductBusiness productBusiness;
    private LinearLayoutManager linearLayoutManager;
    private RecyclerView recyclerView;
    private SessionManager sessionManager;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_list_item_container);
        sessionManager = new SessionManager(this);
        sessionManager.checkLoggin();
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        Realm realm = Realm.getDefaultInstance();

        RealmResults<Product> products;
        productBusiness = new ProductBusiness();
        products = productBusiness.retrieveAllProductsFromDb();

        linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(linearLayoutManager);
    }
}
