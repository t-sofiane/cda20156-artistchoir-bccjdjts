package fr.afpa.cda20156_artistchoir.controller;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;

import java.io.File;

import fr.afpa.cda20156_artistchoir.R;
import fr.afpa.cda20156_artistchoir.beans.Product;
import fr.afpa.cda20156_artistchoir.business.ProductBusiness;
import io.realm.Realm;
import io.realm.RealmRecyclerViewAdapter;
import io.realm.RealmResults;

/**
 * @author jerome
 *
 * L'utilisation de RecyclerView au lieu de ListView pour l'affichage d'une liste
 * est plus optimisé en termes de mémoire
 */
public class ProductListAdapter extends RealmRecyclerViewAdapter<Product, ViewHolder> {

    private Realm realm;
    private Activity context;
    private ViewHolder viewHolder;
    private ImageView edit, delete;
    private ProductBusiness productBusiness;
    private Object ProductListAdapter;

    public ProductListAdapter(RealmResults<Product> list, Activity context) {
        super(list, true, true);
        this.context = context;
        realm = Realm.getDefaultInstance();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.activity_listproducts_main, parent, false);
        return new ViewHolder(itemView);
    }

    /**
     * @author jerome
     *
     * Méthode qui permet de récupérer les variables qui composent chaque item depuis
     * ViewHolder, d'affecter un listener à l'icône delete et de récupérer l'id
     * du produit pour le supprimer dans la méthode deleteProductById de la Classe ProductBusiness
     * @param holder
     * @param position de l'item
     */
    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
        Product product = getItem(position);
        if (product.getPhoto() != null) {
            holder.productImage.setImageURI(Uri.fromFile(new File(product.getPhoto().getLink())));
        }
        holder.productName.setText(product.getName());
        holder.productReference.setText("Ref : "+product.getReference());
        holder.deleteIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                productBusiness = new ProductBusiness();
                productBusiness.deleteProductById(product.getId());
            }
        });

        /**
         * T Sofiane
         * méthode onclick sur le bouton modification
         * qui appelle l'activité de création d'un produit avec un formulaire prérempli
         */
        holder.editIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent( context, CreateProductController.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra("idProduct", (int) product.getId());
                context.startActivity(intent);
            }
        });
    }
}