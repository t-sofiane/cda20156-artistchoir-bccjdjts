package fr.afpa.cda20156_artistchoir.controller;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import java.util.HashMap;

import fr.afpa.cda20156_artistchoir.R;
import fr.afpa.cda20156_artistchoir.beans.Login;
import fr.afpa.cda20156_artistchoir.beans.User;
import fr.afpa.cda20156_artistchoir.business.UserDashboardBusiness;
import fr.afpa.cda20156_artistchoir.session.SessionManager;
import io.realm.Realm;

public class UserDashboardController extends AppCompatActivity {

    private ImageView logo;
    private ImageView logout;
    private Button profile;
    private Button createProduct;
    private Button listProduct;
    private TextView sessionWelcome;
    private Realm realm;
    private UserDashboardBusiness dashboard;
    private User userSession;
    private Login username;
    private SessionManager sessionManager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_dashboard);

        sessionManager = new SessionManager(this); // PERMET DE CHECK LA SESSION - COPIER LES 2 LIGNES dans chaque page hormis page d'accueil
        sessionManager.checkLoggin();

        Bundle extras = new Bundle();

        if(getIntent().getExtras() != null) {
            extras = getIntent().getExtras();
            String connectionSucceed = extras.getString("loginSuccess");
            if (connectionSucceed != null) {
                Toast.makeText(UserDashboardController.this, connectionSucceed, Toast.LENGTH_LONG).show();
            }
            String productCreated = extras.getString("productCreated");
            if (productCreated != null) {
                Toast.makeText(UserDashboardController.this, productCreated, Toast.LENGTH_LONG).show();
            }
            String noProductCreated = extras.getString("NoProductSoFar");
            if (noProductCreated != null) {
                Toast.makeText(UserDashboardController.this, noProductCreated, Toast.LENGTH_LONG).show();
            }
            String IdRetrievedFromProductListItem = extras.getString("IdProduct");
            if (IdRetrievedFromProductListItem != null){
                Toast.makeText(UserDashboardController.this, IdRetrievedFromProductListItem, Toast.LENGTH_LONG).show();
            }
        }

        this.logo = findViewById(R.id.logoDashboard);
        this.logout = findViewById(R.id.logoutImg);
        this.profile = findViewById(R.id.profileBtn);
        this.createProduct = findViewById(R.id.createProductBtn);
        this.listProduct = findViewById(R.id.productListBtn);
        this.sessionWelcome = findViewById(R.id.sessionWelcomeText);

        this.realm = Realm.getDefaultInstance();

        this.dashboard = new UserDashboardBusiness();

        // Va récupérer l'id grâce à SessionManager qui internationalise la session avec l'id, d'où le typage de la valeur du HashMap en Long (voir la classe ConnectionController pour
        // comprendre la construction de la session et la classe SessionManager pour ses configurations)
        userSession = new User();
        HashMap <String,Long> userSess = sessionManager.getSessionDetail();
        userSession.setId(userSess.get(sessionManager.ID));

        // Maintenant qu'on a l'ID, on peut aller récuper le username pour l'afficher dans le dashboard
        username = dashboard.getLoginByUserId(userSession.getId());
        sessionWelcome.setText("Bonjour "+username.getUsername()+", bienvenue sur votre espace personnel");

        this.logo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(UserDashboardController.this, UserDashboardController.class);
                startActivity(intent);
            }
        });

        this.profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(UserDashboardController.this, ProfileController.class);
                intent.putExtra("idUser", (int) username.getId());
                startActivity(intent);
            }
        });

        this.createProduct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(UserDashboardController.this, CreateProductController.class); //Modifier la destination quand la page du profil est faite
                startActivity(intent);
            }
        });

        this.listProduct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(UserDashboardController.this, ProductContainer.class); //Modifier la destination quand la page de liste product est faite
                startActivity(intent);
            }
        });

        this.logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sessionManager.logout();
            }
        });

    }
}