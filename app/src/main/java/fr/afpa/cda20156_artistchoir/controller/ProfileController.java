package fr.afpa.cda20156_artistchoir.controller;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import fr.afpa.cda20156_artistchoir.R;
import fr.afpa.cda20156_artistchoir.beans.Login;
import fr.afpa.cda20156_artistchoir.beans.User;
import fr.afpa.cda20156_artistchoir.business.UserDashboardBusiness;
import fr.afpa.cda20156_artistchoir.session.SessionManager;
import io.realm.Realm;

/***
 * @author T sofiane
 * class Profilecontroller
 */
public class ProfileController extends AppCompatActivity {
    private ImageView logo;
    private ImageView logout;
    private TextView profilMail;
    private TextView profilLogin;
    private Button back, updateUser;

    private Realm realm;
    private UserDashboardBusiness dashboard;
    private User userSession;
    private Login username;
    private SessionManager sessionManager;
    Intent intent = new Intent();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        sessionManager = new SessionManager(this); // PERMET DE CHECK LA SESSION - COPIER LES 2 LIGNES dans chaque page hormis page d'accueil
        sessionManager.checkLoggin();
        this.logo = findViewById(R.id.logoDashboard);
        this.logout = findViewById(R.id.logoutImg);
        this.profilLogin = findViewById(R.id.sessionLogin);
        this.profilMail = findViewById(R.id.sessionMail);
        this.realm = Realm.getDefaultInstance();
        this.back = findViewById(R.id.idBackBtn);
        this.updateUser = findViewById(R.id.idUpdateBtn);

        this.updateUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ProfileController.this, EditProfilController.class);
                intent.putExtra("idUser", (int) username.getId());
                startActivity(intent);
            }
        });
        /**
         * @author T sofiane
         *
         * redirection vers le dashboard
         */
        this.back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ProfileController.this, UserDashboardController.class);
                startActivity(intent);
            }
        });

        /**
         * @author T sofiane
         * Afficher le login et l'adresse mail de l'utilisateur
         *
         */
        this.dashboard = new UserDashboardBusiness();
        intent= getIntent();
        int identifiant= intent.getIntExtra("idUser", -1);
        username = dashboard.getLoginByUserId((long) identifiant);
        this.profilLogin.setText("Login : "+username.getUsername());
        this.profilMail.setText("Mail : "+username.getUser().getEmail());

        /**
         * @author T sofiane
         * méthode de déconnexion
         *
         */
        this.logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sessionManager.logout();
            }
        });
    }
}