package fr.afpa.cda20156_artistchoir.controller;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import fr.afpa.cda20156_artistchoir.MainActivity;
import fr.afpa.cda20156_artistchoir.R;
import fr.afpa.cda20156_artistchoir.beans.Login;
import fr.afpa.cda20156_artistchoir.beans.User;
import fr.afpa.cda20156_artistchoir.business.RegistrationBusiness;
import io.realm.Realm;

/**
 * Contrôleur de l'enregistrement utilisateur
 *
 * @author Cécile
 */
public class RegistrationController extends AppCompatActivity {

    private EditText email;
    private EditText emailCheck;
    private EditText username;
    private EditText password;
    private EditText passwordCheck;
    private Button goBack;
    private Button register;
    private String errors;
    private Realm realm;
    private RegistrationBusiness registrationBusiness;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration_controller);

        Realm.init(getApplicationContext());
        this.realm = Realm.getDefaultInstance();

        this.registrationBusiness = new RegistrationBusiness();

        this.email = findViewById(R.id.id_input_email);
        this.emailCheck = findViewById(R.id.id_input_checkEmail);
        this.username = findViewById(R.id.id_input_username);
        this.password = findViewById(R.id.id_input_password);
        this.passwordCheck = findViewById(R.id.id_input_passwordCheck);

        this.goBack = findViewById(R.id.id_btn_back);
        this.register = findViewById(R.id.id_btn_login);

        this.errors = "";

        this.goBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent().setClass(RegistrationController.this, MainActivity.class));
            }
        });

        this.register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isRegistered()) {
                    Intent intent = new Intent(RegistrationController.this, ConnectionController.class);
                    intent.putExtra("registerSuccess", "Inscription réussie.");
                    startActivity(intent);
                } else {
                    Toast.makeText(RegistrationController.this, errors, Toast.LENGTH_LONG).show();
                    errors = new String();
                }
            }
        });
    }

    /**
     * Enregistre l'utilisateur dans la base de données
     *
     * @return booléen : retourne true si l'enregistrement s'est fait
     */
    private boolean isRegistered() {
        if (this.formIsCompleted()) {

            User user = new User();
            Login login = new Login();

            login.setUsername(username.getText().toString());
            login.setPassword(password.getText().toString());

            user.setEmail(email.getText().toString());
            user.setLogin(login);
            login.setUser(user);

            this.registrationBusiness.saveData(user, login);
            return true;
        }
        return false;
    }

    /**
     * Ensemble de fonctions de vérification.
     *
     * @return booléen : renvoie true si aucune erreur n'a été générée.
     */
    private boolean formIsCompleted() {
        checkEmails(this.email.getText().toString(), this.emailCheck.getText().toString());
        checkUsername(this.username.getText().toString());
        checkPasswords(this.password.getText().toString(), this.passwordCheck.getText().toString());

        if (this.errors.isEmpty()) {
            return true;
        }
        return false;
    }

    /**
     * Vérifie que les emails ont été entrés et sont identiques.
     * @param email : l'email entré par l'utilisateur
     * @param emailCheck : l'email de vérification
     */
    private void checkEmails(String email, String emailCheck) {
        if (email != null && !email.isEmpty() && emailCheck != null && !emailCheck.isEmpty()) {
            if (!emailCheck.equals(email)) {
                this.errors += "Erreur, les emails ne sont pas similaires.\n";
            } else {
                if (this.isEmailStructureValid(email)) {
                    this.checkEmailAvailable(email);
                }
            }
        } else {
            this.errors += "Erreur, veuillez compléter les champs d'email.\n";
        }
    }

    /**
     * Regex vérifiant le format de l'email entré.
     *
     * @param email : l'email à vérifier.
     * @return booléen : retourne true si l'email correspond au format attendu.
     */
    private boolean isEmailStructureValid(String email) {
        Pattern pattern;
        Matcher matcher;
        pattern = Pattern.compile("((^[a-z])[\\W\\w]{1,}[^\\W_]@[0-9a-z]+.[a-z]{2,3}$)");

        matcher = pattern.matcher(email);
        if (!matcher.matches()) {
            this.errors += "Erreur, veuillez entrer un email au format valide.\n";
            return false;
        } else {
            return true;
        }
    }

    /**
     * Vérifie la disponibilité de l'email.
     *
     * @param email : l'email à vérifier.
     */
    private void checkEmailAvailable(String email) {
        if (this.registrationBusiness.getUserByEmail(email) != null) {
            this.errors += "Erreur, email non disponible.\n";
        }
    }

    /**
     * Vérifie que le pseudo aa bien été entré.
     *
     * @param username : le pseudo à vérifier.
     */
    private void checkUsername(String username) {
        if (username == null || username.isEmpty()) {
            this.errors += "Erreur, veuillez compléter le champ de pseudo.\n";
        } else {
            this.checkUsernameAvailable(username);
        }
    }

    /**
     * Vérifie la disponibilité du pseudo entré par l'utilisateur.
     *
     * @param username : le pseudo à vérifier.
     */
    private void checkUsernameAvailable(String username) {
        if (this.registrationBusiness.getLoginByUsername(username) != null) {
            this.errors += "Erreur, pseudo non disponible.\n";
        }
    }

    /**
     * Vérifie que les mots de passe sont présents et identiques
     *
     * @param password : le mot de passe entré par l'utilisateur.
     * @param passwordCheck : la vérification du mot de passe.
     */
    private void checkPasswords(String password, String passwordCheck) {
        if (password != null && !password.isEmpty() && passwordCheck != null && !passwordCheck.isEmpty()) {
            if (!passwordCheck.equals(password)) {
                this.errors += "Erreur, les mots de passe ne sont pas similaires.\n";
            } else if (passwordCheck.equals(password) && password.length() < 6) {
                this.errors += "Erreur, le mot de passe doit faire 6 caractères au minimum.\n";
            }
        } else {
            this.errors += "Erreur, veuillez compléter les champs de mot de passe.\n";
        }
    }
}