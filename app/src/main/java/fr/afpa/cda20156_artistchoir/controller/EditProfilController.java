package fr.afpa.cda20156_artistchoir.controller;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import fr.afpa.cda20156_artistchoir.R;
import fr.afpa.cda20156_artistchoir.beans.Login;
import fr.afpa.cda20156_artistchoir.beans.User;
import fr.afpa.cda20156_artistchoir.business.RegistrationBusiness;
import fr.afpa.cda20156_artistchoir.business.UserDashboardBusiness;
import fr.afpa.cda20156_artistchoir.session.SessionManager;
import io.realm.Realm;

/**
 * @author T sofiane
 * la classe de modification des informations de l'utilisateur
 */

public class EditProfilController extends AppCompatActivity {
    private EditText email;
    private EditText emailCheck;
    private EditText pseudo;
    private EditText password;
    private EditText passwordCheck;
    private String errors;
    private Realm realm;
    private ImageView logo;
    private ImageView logout;
    private Button back, updateUser;
    private int identifiant;
    private RegistrationBusiness registrationBusiness;
    private UserDashboardBusiness dashboard;
    private User userSession;
    private Login username;
    private SessionManager sessionManager;
    Intent intent = new Intent();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profil);
        sessionManager = new SessionManager(this); // PERMET DE CHECK LA SESSION - COPIER LES 2 LIGNES dans chaque page hormis page d'accueil
        sessionManager.checkLoggin();
        this.logo = findViewById(R.id.logoDashboard);
        this.logout = findViewById(R.id.logoutImg);
        this.realm = Realm.getDefaultInstance();
        this.back = findViewById(R.id.idBackBtn);
        this.email = findViewById(R.id.id_input_email);
        this.emailCheck = findViewById(R.id.id_input_checkEmail);
        this.pseudo = findViewById(R.id.id_input_username);
        this.password = findViewById(R.id.id_input_password);
        this.passwordCheck = findViewById(R.id.id_input_passwordCheck);
        this.updateUser = findViewById(R.id.id_btn_update);

        this.registrationBusiness = new RegistrationBusiness();

        /**
         * @author T sofiane
         * Préremplir le formulaire de modification des infos utilisateur
         *
         */
        this.dashboard = new UserDashboardBusiness();
        intent= getIntent();
        identifiant= intent.getIntExtra("idUser", -1);
        username = dashboard.getLoginByUserId((long) identifiant);
        this.pseudo.setText(username.getUsername());
        this.email.setText(username.getUser().getEmail());
        this.emailCheck.setText(username.getUser().getEmail());
        this.password.setText(username.getPassword());
        this.passwordCheck.setText(username.getPassword());

        this.updateUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isRegistered()) {
                    Intent intent = new Intent(EditProfilController.this, ConnectionController.class);
                    intent.putExtra("registerSuccess", "Mise à jour réussie.");
                    startActivity(intent);
                } else {
                    Toast.makeText(EditProfilController.this, errors, Toast.LENGTH_LONG).show();
                    errors = new String();
                }
            }
        });
        /**
         * @author T sofiane
         *
         * redirection vers le dashboard
         */
        this.back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(EditProfilController.this, UserDashboardController.class);
                startActivity(intent);
            }
        });

        /**
         * méthode de déconnexion
         *
         */
        this.logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sessionManager.logout();
            }
        });
    }

    /**
     * Enregistre l'utilisateur dans la base de données
     *
     * @return booléen : retourne true si l'enregistrement s'est fait
     */
    private boolean isRegistered() {
        if (this.formIsCompleted()) {

            User user = new User();
            Login login = new Login();
            login.setId(identifiant);
            login.setUsername(pseudo.getText().toString());
            login.setPassword(password.getText().toString());
            user.setId(identifiant);
            user.setEmail(email.getText().toString());
            user.setLogin(login);
            login.setUser(user);

            this.registrationBusiness.updateData(user, login);
            return true;
        }
        return false;
    }

    /**
     * Ensemble de fonctions de vérification.
     *
     * @return booléen : renvoie true si aucune erreur n'a été générée.
     */
    private boolean formIsCompleted() {

        if(!(username.getUser().getEmail().equals(this.email.getText().toString())
           &&username.getUser().getEmail().equals(this.emailCheck.getText().toString()))){
            checkEmails(this.email.getText().toString(), this.emailCheck.getText().toString());
        }
        if(!username.getUsername().equals(this.pseudo.getText().toString())){
            checkUsername(this.pseudo.getText().toString());
        }
        checkPasswords(this.password.getText().toString(), this.passwordCheck.getText().toString());

        if (errors==null) {
            return true;
        }
        return false;
    }

    /**
     * Vérifie que les emails ont été entrés et sont identiques.
     * @param email : l'email entré par l'utilisateur
     * @param emailCheck : l'email de vérification
     */
    private void checkEmails(String email, String emailCheck) {
        if (email != null && !email.isEmpty() && emailCheck != null && !emailCheck.isEmpty()) {
            if (!emailCheck.equals(email)) {
                this.errors += "Erreur, les emails ne sont pas similaires.\n";
            } else {
                if (this.isEmailStructureValid(email)) {
                    this.checkEmailAvailable(email);
                }
            }
        } else {
            this.errors += "Erreur, veuillez compléter les champs d'email.\n";
        }
    }

    /**
     * Regex vérifiant le format de l'email entré.
     *
     * @param email : l'email à vérifier.
     * @return booléen : retourne true si l'email correspond au format attendu.
     */
    private boolean isEmailStructureValid(String email) {
        Pattern pattern;
        Matcher matcher;
        pattern = Pattern.compile("((^[a-z])[\\W\\w]{1,}[^\\W_]@[0-9a-z]+.[a-z]{2,3}$)");

        matcher = pattern.matcher(email);
        if (!matcher.matches()) {
            this.errors += "Erreur, veuillez entrer un email au format valide.\n";
            return false;
        } else {
            return true;
        }
    }

    /**
     * Vérifie la disponibilité de l'email.
     *
     * @param email : l'email à vérifier.
     */
    private void checkEmailAvailable(String email) {
        if (this.registrationBusiness.getUserByEmail(email) != null) {
            this.errors += "Erreur, email non disponible.\n";
        }
    }

    /**
     * Vérifie que le pseudo a bien été entré.
     *
     * @param username : le pseudo à vérifier.
     */
    private void checkUsername(String username) {
        if (username == null || username.isEmpty()) {
            this.errors += "Erreur, veuillez compléter le champ de pseudo.\n";
        } else {
            this.checkUsernameAvailable(username);
        }
    }

    /**
     * Vérifie la disponibilité du pseudo entré par l'utilisateur.
     *
     * @param username : le pseudo à vérifier.
     */
    private void checkUsernameAvailable(String username) {
        if (this.registrationBusiness.getLoginByUsername(username) != null) {
            this.errors += "Erreur, pseudo non disponible.\n";
        }
    }

    /**
     * Vérifie que les mots de passe sont présents et identiques
     *
     * @param password : le mot de passe entré par l'utilisateur.
     * @param passwordCheck : la vérification du mot de passe.
     */
    private void checkPasswords(String password, String passwordCheck) {
        if (password != null && !password.isEmpty() && passwordCheck != null && !passwordCheck.isEmpty()) {
            if (!passwordCheck.equals(password)) {
                this.errors += "Erreur, les mots de passe ne sont pas similaires.\n";
            } else if (passwordCheck.equals(password) && password.length() < 6) {
                this.errors += "Erreur, le mot de passe doit faire 6 caractères au minimum.\n";
            }
        } else {
            this.errors += "Erreur, veuillez compléter les champs de mot de passe.\n";
        }
    }

}