package fr.afpa.cda20156_artistchoir.controller;

import android.annotation.SuppressLint;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.provider.OpenableColumns;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Date;
import java.util.HashMap;
import java.util.Optional;

import fr.afpa.cda20156_artistchoir.R;
import fr.afpa.cda20156_artistchoir.beans.Product;
import fr.afpa.cda20156_artistchoir.beans.User;
import fr.afpa.cda20156_artistchoir.business.ProductBusiness;
import fr.afpa.cda20156_artistchoir.session.SessionManager;
import io.realm.Realm;

public class CreateProductController extends AppCompatActivity {

    private TextInputLayout idNameInputLayout, idBrandInputLayout, idDescInputLayout, idRefInputLayout;
    private TextInputEditText editName, editBrand, editDesc, editRef;
    private ImageView logo, logout;
    private boolean isSaveWorking;
    private Date creationDate;
    private String productName, productBrand, productDescription, productRef;
    private Button back, createProduct;
    private User user;
    private SessionManager sessionManager;
    private Realm realm;
    private ImageView imageView;
    private ProductBusiness productBusiness;
    private static final int REQUEST_IMAGE_CAPTURE=101;
    private boolean visible = true;
    private String photoPath;
    private Uri photoUri;
    private int idProduct;
    private Intent intent;
    private Product product;
    private static Uri contentUri = null;


    /**
     * @author : jerome
     * Utilisation du couple TextInputLayout / TextInputEditText -> permet de mieux traiter la partie contrôles / erreurs
     * sessionManager vérifie si la session est toujours active. Sinon, renvoyé vers la page d'accueil
     * sessionManager a également la vocation de faire transiter l'id de l'utilisateur de la session pour affecter le bon utilisateur au produit créé (voir la classe ProductBusiness)
     *
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_product);
        Realm.init(getApplicationContext());
        this.realm = Realm.getDefaultInstance();
        System.out.println("CREATE PRODUCT CONTROLLER ********************************************");
        sessionManager = new SessionManager(this); // PERMET DE CHECK LA SESSION - COPIER LES 2 LIGNES dans chaque page hormis page d'accueil
        sessionManager.checkLoggin();

        this.logo = findViewById(R.id.logoDashboard);
        this.logout = findViewById(R.id.logoutImg);

        this.idNameInputLayout = findViewById(R.id.idNameInputLayout);
        this.idBrandInputLayout = findViewById(R.id.idBrandInputLayout);
        this.idDescInputLayout = findViewById(R.id.idDescInputLayout);
        this.idRefInputLayout = findViewById(R.id.idRefInputLayout);

        editName = findViewById(R.id.idName);
        editBrand = findViewById(R.id.idBrand);
        editDesc = findViewById(R.id.idDescription);
        editRef = findViewById(R.id.idReference);

        imageView = findViewById(R.id.ImageView);

        this.back = findViewById(R.id.idBackBtn);
        this.createProduct = findViewById(R.id.idCreateBtn);

        this.productBusiness = new ProductBusiness();

        user = new User();
        HashMap<String,Long> userSess = sessionManager.getSessionDetail();
        user.setId(userSess.get(sessionManager.ID));

        // on récupére les informations du produit, s'il s'agit d'une modification d'un produit
        intent = getIntent();
        idProduct= intent.getIntExtra("idProduct", -1);
        Product product=productBusiness.getProductById((long)idProduct);
        if(product!=null){
            editName.setText(product.getName());
            editBrand.setText(product.getBrand());
            editDesc.setText(product.getDescription());
            editRef.setText(product.getReference());
        }


        /**
         * @author : jerome
         *
         * Si l'user appuye sur back, redirigé vers le dashboard
         */
        this.back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(CreateProductController.this, UserDashboardController.class);
                startActivity(intent);
            }
        });

        /**
         * @author : jerome
         *
         * Si l'user appuye sur le logo, redirigé vers le dashboard
         */
        this.logo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(CreateProductController.this, UserDashboardController.class);
                startActivity(intent);
            }
        });

        /**
         * @author : jerome
         *
         * Si l'user appuye sur le bouton de déconnexion, appel de la méthode logout() de la classe SessionManager pour logout le user et
         * la redirection vers la page d'accueil est directement dans la méthode logout() à des fins d'optimisation
         */
        this.logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sessionManager.logout();
            }
        });

        /**
         * @author : jerome
         * Méthode qui permet de d'appeler la méthode globale de contrôle isFormOk(), puis si true d'appeler la méthode de save du produit
         * Si la méthode saveProduct() renvoie un true, le produit est créé et il faut supprimer
         *  manuellement les éventuels messages d'erreurs de TextInputLayout grâce à la méthode .setError(null)
         *
         */
        this.createProduct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isFormOk()){
                    creationDate = new Date();
                    if (saveProductController(creationDate)) {
                        idNameInputLayout.setError(null);
                        idBrandInputLayout.setError(null);
                        idDescInputLayout.setError(null);
                        idRefInputLayout.setError(null);
                        Intent intent = new Intent(CreateProductController.this, UserDashboardController.class);
                        intent.putExtra("productCreated", "Le produit est publié");
                        startActivity(intent);
                    }
                }
            }
        });
    }

    public void requestFocus (View view){
        if (view.requestFocus()){
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    /**
     * @author jerome - Cette méthode permet vérifier si la création du produit a réussi. Retourne vrai ou faux selon le résultat, méthode appelée dans
     * onClick() du bouton createProduct
     *
     * @param creationDate
     * @return true ou false
     */
    public boolean saveProductController(Date creationDate){
        boolean isProductCreated = true;
        try {
            //si idProduct = -1 on enregisitre un nouveau produit, sinon c'est une mise à jour d'un produit
            if(idProduct==-1){
                productBusiness.saveProduct(creationDate, productName, productBrand, productDescription, productRef, user, photoPath);
            }else {
                productBusiness.updateProduct(idProduct, productName, productBrand, productDescription, productRef);

            }
        } catch (Exception e) {
            System.out.println("ERROR SAVING PRODUCT : "+e);
            isProductCreated = false;
        }
        return isProductCreated;
    }

    /**
     * @author jerome - Méthode qui teste le retour de tous les champs. Si une seule méthode parmi la liste est fausse, le retour de cette méthode est
     * false. La méthode est appelée dans le onClick() du bouton createProduct()
     *
     * @return true ou false
     */
    private boolean isFormOk(){
        boolean areProductFieldsOk = true;
        if (!isNameValid(editName.getText().toString().trim())){
            areProductFieldsOk = false;
            idNameInputLayout.setError("Le nom de produit est obligatoire");
            requestFocus(editName);
        } else {
            idNameInputLayout.setError(null);
        }
        if (!isProductBrandValid(editBrand.getText().toString().trim())){
            areProductFieldsOk = false;
            idBrandInputLayout.setError("La marque doit apparaître dans la fiche produit");
            requestFocus(editBrand);
        } else {
            idBrandInputLayout.setError(null);
        }
        if (!isProductDescOk(editDesc.getText().toString().trim())){
            areProductFieldsOk = false;
            idDescInputLayout.setError("Renseignez une description pour les autres utilisateurs");
            requestFocus(editDesc);
        } else {
            idDescInputLayout.setError(null);
        }
        if (!isReferenceDigit(editRef.getText().toString().trim())){
            areProductFieldsOk = false;
            idRefInputLayout.setError("La référence doit contenir seulement des chiffres");
            requestFocus(editRef);
        }
        if (!isReferenceOk(editRef.getText().toString().trim())){
            areProductFieldsOk = false;
            idRefInputLayout.setError("La référence produit est obligatoire pour l'identification");
            requestFocus(editRef);
        }

        //si c'est un nouveau produit on verifie l'unicité de la référence
        if (idProduct==-1) {
            Optional<Product> productRef = productBusiness.retrieveProductByReference(editRef.getText().toString().trim());
            if (productRef.isPresent()) {
                areProductFieldsOk = false;
                idRefInputLayout.setError("La référence produit existe déjà, elle doit être unique");
                requestFocus(editRef);
            }
        }
        return areProductFieldsOk;
    }

    /**
     * @author jerome - Contrôle de nullité du nom
     *
     * @param name nom
     * @return true ou false
     */
    private boolean isNameValid(String name) {
        boolean isNameOk = true;
        productName = name;
        if (name.isEmpty() || name == null) {
            isNameOk = false;
        }
        return isNameOk;
    }

    /**
     * @author jerome - Contrôle de nullité de la marque
     *
     * @param brand marque
     * @return true ou false
     */
    private boolean isProductBrandValid(String brand) {
        boolean isBrandOk = true;
        productBrand = brand;
        if (brand == null || brand.isEmpty()){
            isBrandOk = false;
        }
        return isBrandOk;
    }

    /**
     * @author jerome - Contrôle de nullité de la description
     *
     * @param description description
     * @return true ou false
     */
    private boolean isProductDescOk(String description) {
        boolean isDescriptionOk = true;
        productDescription = description;
        if (description == null || description.isEmpty()){
            isDescriptionOk = false;
        }
        return isDescriptionOk;
    }

    /**
     * @author jerome - Contrôle de nullité de la référence
     *
     * @param ref référence
     * @return true ou false
     */
    private boolean isReferenceOk(String ref) {
        boolean isRefOk = true;
        productRef = ref;
        if (ref == null || ref.isEmpty()){
            isRefOk = false;
        }
        return isRefOk;
    }

    /**
     * @author jerome - Contrôle de format de la référence - Doit être numérique seulement
     *
     * @param reference
     * @return true ou false
     */
    private boolean isReferenceDigit(String reference) {
        boolean isRefDigit = true;
        if (!reference.matches("[0-9]+") || reference.length() != 13) {
            isRefDigit = false;
        }
        return isRefDigit;
    }

    /**
     * @author Julien
     *
     * Méthode permettant de récupérer l'image prise à partir de l'appareil photo du téléphone
     * @param view qui déclenche au clic l'application native de la camera
     */
    public void photo(View view) {

        Intent imageTakeIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        if(imageTakeIntent.resolveActivity(getPackageManager())!=null){

            startActivityForResult(imageTakeIntent, REQUEST_IMAGE_CAPTURE);
        }
    }

    /**
     * @author Julien
     *
     * Méthode permettant de récupérer uri à partir du bitmap de l'image, de l'envoyer en traitement à la méthode getPath()
     * pour récupérer le chemin ligne 383 et exécuter son enregistrement dans l'appareil ligne 384
     * @param requestCode
     * @param resultCode
     * @param data
     */
    @SuppressLint("MissingSuperCall")
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {

            Bundle extras = data.getExtras();
            Bitmap imageBitmap = (Bitmap) extras.get("data");
            imageView.setImageBitmap(imageBitmap);
            photoUri = getImageUri(getApplicationContext(), imageBitmap);
            photoPath = getPath(getApplicationContext(), photoUri);
            tryToSaveImage(imageBitmap);
            //String adresse = getImageUri( CreateProductController.this, imageBitmap).toString();
            //Photo picture = new Photo(adresse, visible );
        }
    }

    /**
     * @author Julien
     *
     * Méthode qui permet l'enregistrement de la photo à partir de son chemin
     * @param image Bitmap de l'image
     */
    private void tryToSaveImage(Bitmap image) {
        try {
            int quality = 100;
            FileOutputStream fos = new FileOutputStream(new File(String.valueOf(photoPath)));
            image.compress(Bitmap.CompressFormat.PNG, quality, fos);
            fos.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * @author Julien
     *
     * Méthode qui prend en paramètre le bitmap de l'image, le contexte de l'application, compresser l'image et récupérer le chemin ensuite parsé en uri.
     * @param inContext Contexte de l'application
     * @param inImage Bitmap de l'image
     * @return uri de l'image
     */
    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.PNG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(getContentResolver(), inImage, "Title", "description");
        return Uri.parse(path);
    }

    /**
     * @author Julien
     *
     * Méthode globale qui prend en paramètre l'uri et vérifie son authority et en fonction renvoie le chemin de la photo
     * @param context de l'application
     * @param uri de la photo
     * @return Chemin de la photo en String
     */
    @SuppressLint("NewApi")
    public static String getPath(final Context context, final Uri uri) {
        // check here to KITKAT or new version
        final boolean isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;
        String selection = null;
        String[] selectionArgs = null;
        // DocumentProvider
        if (isKitKat && DocumentsContract.isDocumentUri(context, uri)) {
            // ExternalStorageProvider
            if (isExternalStorageDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                String fullPath = getPathFromExtSD(split);
                if (fullPath != "") {
                    return fullPath;
                } else {
                    return null;
                }
            }

            // DownloadsProvider
            else if (isDownloadsDocument(uri)) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    final String id;
                    Cursor cursor = null;
                    try {
                        cursor = context.getContentResolver().query(uri, new String[]{MediaStore.MediaColumns.DISPLAY_NAME}, null, null, null);
                        if (cursor != null && cursor.moveToFirst()) {
                            String fileName = cursor.getString(0);
                            String path = Environment.getExternalStorageDirectory().toString() + "/Download/" + fileName;
                            if (!TextUtils.isEmpty(path)) {
                                return path;
                            }
                        }
                    } finally {
                        if (cursor != null)
                            cursor.close();
                    }
                    id = DocumentsContract.getDocumentId(uri);
                    if (!TextUtils.isEmpty(id)) {
                        if (id.startsWith("raw:")) {
                            return id.replaceFirst("raw:", "");
                        }
                        String[] contentUriPrefixesToTry = new String[]{
                                "content://downloads/public_downloads",
                                "content://downloads/my_downloads"
                        };
                        for (String contentUriPrefix : contentUriPrefixesToTry) {
                            try {
                                final Uri contentUri = ContentUris.withAppendedId(Uri.parse(contentUriPrefix), Long.valueOf(id));

                         /*   final Uri contentUri = ContentUris.withAppendedId(
                                    Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));*/

                                return getDataColumn(context, contentUri, null, null);
                            } catch (NumberFormatException e) {
                                //In Android 8 and Android P the id is not a number
                                return uri.getPath().replaceFirst("^/document/raw:", "").replaceFirst("^raw:", "");
                            }
                        }


                    }

                } else {
                    final String id = DocumentsContract.getDocumentId(uri);
                    final boolean isOreo = Build.VERSION.SDK_INT >= Build.VERSION_CODES.O;
                    if (id.startsWith("raw:")) {
                        return id.replaceFirst("raw:", "");
                    }
                    try {
                        contentUri = ContentUris.withAppendedId(
                                Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));

                    } catch (NumberFormatException e) {
                        e.printStackTrace();
                    }
                    if (contentUri != null) {
                        return getDataColumn(context, contentUri, null, null);
                    }
                }


            }
            // MediaProvider
            else if (isMediaDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                Uri contentUri = null;

                if ("image".equals(type)) {
                    contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                } else if ("video".equals(type)) {
                    contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                } else if ("audio".equals(type)) {
                    contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                }
                selection = "_id=?";
                selectionArgs = new String[]{split[1]};


                return getDataColumn(context, contentUri, selection,
                        selectionArgs);
            } else if (isGoogleDriveUri(uri)) {
                return getDriveFilePath(uri, context);
            }
        }


        // MediaStore (and general)
        else if ("content".equalsIgnoreCase(uri.getScheme())) {

            if (isGooglePhotosUri(uri)) {
                return uri.getLastPathSegment();
            }

            if (isGoogleDriveUri(uri)) {
                return getDriveFilePath(uri, context);
            }
            if( Build.VERSION.SDK_INT == Build.VERSION_CODES.N)
            {
                // return getFilePathFromURI(context,uri);
                return getMediaFilePathForN(uri, context);
                // return getRealPathFromURI(context,uri);
            }else
            {

                return getDataColumn(context, uri, null, null);
            }


        }
        // File
        else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }

        return null;
    }

    /**
     * @author Julien
     * Vérifie si le fichier existe dans l'appareil
     *
     * @param filePath Chemin absolu du fichier
     * @return True si le fichier existe, false sinon
     */
    private static boolean fileExists(String filePath) {
        File file = new File(filePath);

        return file.exists();
    }


    /**
     * @author Julien
     *
     * Cette méthode renvoie le chemin si l'authority de l'uri est lié à ExternalStorageProvider
     * @param pathData Type de stockage et chemin relatif
     * @return Chemin du fichier depuis la carte SD
     */
    private static String getPathFromExtSD(String[] pathData) {
        final String type = pathData[0];
        final String relativePath = "/" + pathData[1];
        String fullPath = "";

        // on my Sony devices (4.4.4 & 5.1.1), `type` is a dynamic string
        // something like "71F8-2C0A", some kind of unique id per storage
        // don't know any API that can get the root path of that storage based on its id.
        //
        // so no "primary" type, but let the check here for other devices
        if ("primary".equalsIgnoreCase(type)) {
            fullPath = Environment.getExternalStorageDirectory() + relativePath;
            if (fileExists(fullPath)) {
                return fullPath;
            }
        }

        // Environment.isExternalStorageRemovable() is `true` for external and internal storage
        // so we cannot relay on it.
        //
        // instead, for each possible path, check if file exists
        // we'll start with secondary storage as this could be our (physically) removable sd card
        fullPath = System.getenv("SECONDARY_STORAGE") + relativePath;
        if (fileExists(fullPath)) {
            return fullPath;
        }

        fullPath = System.getenv("EXTERNAL_STORAGE") + relativePath;
        if (fileExists(fullPath)) {
            return fullPath;
        }

        return fullPath;
    }

    /**
     * @author Julien
     *
     * Si l'authority de l'uri correspond à "com.google.android.apps.docs.storage", cette méthode est appelée. Ce n'est pas notre cas.
     * @param uri de la photo
     * @param context de l'application
     * @return chemin de la photo
     */
    private static String getDriveFilePath(Uri uri, Context context) {
        Uri returnUri = uri;
        Cursor returnCursor = context.getContentResolver().query(returnUri, null, null, null, null);
        /*
         * Get the column indexes of the data in the Cursor,
         *     * move to the first row in the Cursor, get the data,
         *     * and display it.
         * */
        int nameIndex = returnCursor.getColumnIndex(OpenableColumns.DISPLAY_NAME);
        int sizeIndex = returnCursor.getColumnIndex(OpenableColumns.SIZE);
        returnCursor.moveToFirst();
        String name = (returnCursor.getString(nameIndex));
        String size = (Long.toString(returnCursor.getLong(sizeIndex)));
        File file = new File(context.getCacheDir(), name);
        try {
            InputStream inputStream = context.getContentResolver().openInputStream(uri);
            FileOutputStream outputStream = new FileOutputStream(file);
            int read = 0;
            int maxBufferSize = 1 * 1024 * 1024;
            int bytesAvailable = inputStream.available();

            //int bufferSize = 1024;
            int bufferSize = Math.min(bytesAvailable, maxBufferSize);

            final byte[] buffers = new byte[bufferSize];
            while ((read = inputStream.read(buffers)) != -1) {
                outputStream.write(buffers, 0, read);
            }
            Log.e("File Size", "Size " + file.length());
            inputStream.close();
            outputStream.close();
            Log.e("File Path", "Path " + file.getPath());
            Log.e("File Size", "Size " + file.length());

        } catch (Exception e) {
            Log.e("Exception", e.getMessage());
        }
        return file.getPath();
    }

    /**
     * @author Julien
     *
     * Méthode permettant de retourner le chemin en String du fichier en prenant en paramètre son URI et le contexte de l'application
     * @param uri de la photo
     * @param context de l'application
     * @return Chemin de la photo
     */
    private static String getMediaFilePathForN(Uri uri, Context context) {
        Uri returnUri = uri;
        Cursor returnCursor = context.getContentResolver().query(returnUri, null, null, null, null);
        /*
         * Get the column indexes of the data in the Cursor,
         *     * move to the first row in the Cursor, get the data,
         *     * and display it.
         * */
        int nameIndex = returnCursor.getColumnIndex(OpenableColumns.DISPLAY_NAME);
        int sizeIndex = returnCursor.getColumnIndex(OpenableColumns.SIZE);
        returnCursor.moveToFirst();
        String name = (returnCursor.getString(nameIndex));
        String size = (Long.toString(returnCursor.getLong(sizeIndex)));
        File file = new File(context.getFilesDir(), name);
        try {
            InputStream inputStream = context.getContentResolver().openInputStream(uri);
            FileOutputStream outputStream = new FileOutputStream(file);
            int read = 0;
            int maxBufferSize = 1 * 1024 * 1024;
            int bytesAvailable = inputStream.available();

            //int bufferSize = 1024;
            int bufferSize = Math.min(bytesAvailable, maxBufferSize);

            final byte[] buffers = new byte[bufferSize];
            while ((read = inputStream.read(buffers)) != -1) {
                outputStream.write(buffers, 0, read);
            }
            inputStream.close();
            outputStream.close();
        } catch (Exception e) {
            Log.e("Exception", e.getMessage());
        }
        return file.getPath();
    }


    private static String getDataColumn(Context context, Uri uri,
                                        String selection, String[] selectionArgs) {
        Cursor cursor = null;
        final String column = "_data";
        final String[] projection = {column};

        try {
            cursor = context.getContentResolver().query(uri, projection,
                    selection, selectionArgs, null);

            if (cursor != null && cursor.moveToFirst()) {
                final int index = cursor.getColumnIndexOrThrow(column);
                return cursor.getString(index);
            }
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return null;
    }

    /**
     * @param uri - The Uri to check.
     * @return - Whether the Uri authority is ExternalStorageProvider.
     */
    private static boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri - The Uri to check.
     * @return - Whether the Uri authority is DownloadsProvider.
     */
    private static boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri - The Uri to check.
     * @return - Whether the Uri authority is MediaProvider.
     */
    private static boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri - The Uri to check.
     * @return - Whether the Uri authority is Google Photos.
     */
    private static boolean isGooglePhotosUri(Uri uri) {
        return "com.google.android.apps.photos.content".equals(uri.getAuthority());
    }


    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is Google Drive.
     */
    private static boolean isGoogleDriveUri(Uri uri) {
        return "com.google.android.apps.docs.storage".equals(uri.getAuthority()) || "com.google.android.apps.docs.storage.legacy".equals(uri.getAuthority());
    }
}