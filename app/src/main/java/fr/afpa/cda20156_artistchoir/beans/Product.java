package fr.afpa.cda20156_artistchoir.beans;


import java.util.Date;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor
@ToString
public class Product extends RealmObject {

    @PrimaryKey
    private long id;
    private String reference;
    private String name;
    private String brand;
    private String description;
    private Date creationDate;
    private User author;
    private Photo photo;


    public Product(String reference, String name, String brand, String description, Date creationDate, User author) {
        this.reference = reference;
        this.name = name;
        this.brand = brand;
        this.description = description;
        this.creationDate = creationDate;
        this.author = author;
    }

    public Product() {
    }


}
