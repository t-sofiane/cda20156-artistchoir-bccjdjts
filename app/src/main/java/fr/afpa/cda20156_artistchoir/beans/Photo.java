package fr.afpa.cda20156_artistchoir.beans;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor
@ToString
public class Photo extends RealmObject {

    @PrimaryKey
    private long id;
    private String link;
    private boolean active;


    public Photo(String link, boolean active) {
        this.link = link;
        this.active = active;
    }

    public Photo() {
    }
}
