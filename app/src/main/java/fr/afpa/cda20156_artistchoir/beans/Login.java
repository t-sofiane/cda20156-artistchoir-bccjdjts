package fr.afpa.cda20156_artistchoir.beans;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor
@ToString
public class Login extends RealmObject {

    @PrimaryKey
    private long id;
    private String username;
    private String password;
    private User user;


    public Login(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public Login() {
    }
}
