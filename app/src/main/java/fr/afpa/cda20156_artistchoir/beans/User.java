package fr.afpa.cda20156_artistchoir.beans;


import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor
@ToString
public class User extends RealmObject {

    @PrimaryKey
    private long id;
    private String email;
    private boolean active;
    private Login login;
    private Photo photo;


    public User(String email, boolean active, Login login) {
        this.email = email;
        this.active = active;
        this.login = login;
    }

    public User() {
    }
}
