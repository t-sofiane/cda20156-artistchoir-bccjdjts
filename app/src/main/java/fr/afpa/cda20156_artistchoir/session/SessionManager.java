package fr.afpa.cda20156_artistchoir.session;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import java.util.HashMap;

import fr.afpa.cda20156_artistchoir.MainActivity;

/**
 * @author jerome
 *
 * La classe SessionManager a vocation à gérer la session, par l'utilisation de l'interface SharedPreferences
 * Plusieurs méthodes y sont créées puis appelées dans les view, notamment checkLoggin() systématiquement à chaque début de controller de View pour
 * check si une session est bien active, à moins qu'une view soit laissée accessible à tous utilisateurs, dont ceux non connectés
 */
public class SessionManager {

    SharedPreferences sharedPreferences;
    public SharedPreferences.Editor editor;
    public Context context;

    private static final String PREF_NAME = "LOGIN";
    private static final String LOGIN = "IS_LOGIN";
    public static final String ID = "ID_KEY";

    /**
     * @author jerome
     *
     * Constructeur de la classe SessionManager. On y passe en paramètre le contexte (la classe active) pour ensuite pouvoir utiliser les autres méthodes de la classe SessionManager.
     * @param context view active
     */
    public SessionManager(Context context){
        this.context = context;
        sharedPreferences = context.getSharedPreferences("AppKey",Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
    }

    /**
     * @author jerome
     *
     * On doit y passer en paramètre la session à créer. Méthode appelée au moment de l'authentification, après avoir checké dans la DB si le username était bien
     * en DB et le mot de passe est ok.
     * Il faut ici set editor avec un booleen de connexion à true, l'ID de l'user et commit avec apply()
     * @param sessionId id de la session
     */
    public void createSession(Long sessionId){
        editor.putBoolean(LOGIN, true);
        editor.putLong(ID, sessionId);
        editor.apply();
    }

    /**
     * @author jerome
     *
     * Retourne vrai si session active, faux sinon. Par défaut sur faux pour une meilleure sécurité. Cette méthode est appelée dans la méthode checkLoggin()
     * @return
     */
    public boolean isLoggin(){
        return sharedPreferences.getBoolean(LOGIN, false);
    }

    /**
     * @uthor jerome
     *
     * Méthode appelée dans toutes les view à limiter aux utilisateurs dont la session doit être active.
     */
    public void checkLoggin(){
        if (!this.isLoggin()){
            editor.clear();
            editor.commit();
            logout();
        }
    }

    /**
     * @author jerome
     *
     * Il convient ici de faire une Map de l'ID de l'user afin de récupérer cette valeur dans toute view où cette méthode est appelée
     * @return
     */
    public HashMap<String, Long> getSessionDetail(){
        HashMap<String, Long> userSession = new HashMap<>();
        userSession.put(ID, sharedPreferences.getLong(ID, 0L));
        return userSession;
    }

    /**
     * @author jerome
     *
     * Méthode qui permet de logout() l'utilisateur, selon le scénario en fonction de s'il a choisit de se logout ou si la session a été perdue,
     * par exemple si la méthode isLoggin() retourne false quand elle appelée par checkLoggin() dans une view sécurisée
     * Si logout() est appelé, l'utilisateur est redirigé vers MainActivity et doit s'authentifier de nouveau pour accéder à son espace personnel
     */
    public void logout(){
        editor.clear();
        editor.commit();
        Intent intent = new Intent(context, MainActivity.class);
        context.startActivity(intent);
    }
}