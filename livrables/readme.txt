﻿Merci d'avoir choisi l'application Choeur d'artistes chauds

--------------------------------------
******* Installer l'application *******
--------------------------------------

L'application ne fonctionne que sur l'OS Android à partir de la version 24.
Activez le mode développeur sur votre smartphone Android :
- Aller dans Paramètres > Système > A propos de l'appareil. Touchez 7 fois le numéro de build

Puis :

- Ouvrir le dossier cda20156-artistchoir-bccjdjts reçu de notre part
- Ouvrir le sous-dossier livrables > apk > release > Copier le fichier app-release.apk

- Brancher votre smartphone sur votre PC à l'aide d'un câble USB
- Permettre depuis votre smartphone l'échange des données entre vos 2 terminaux (Tapez sur OK sur votre smartphone)
- Ouvrir l'explorateur de fichier sur votre PC
- Cliquer sur "Ce PC" et localisez votre smartphone par son nom
- Double clic sur ce phériphérique

- Coller le fichier app-release.apk dans un des dossiers de votre smartphone, assurez de pouvoir le retrouver
facilement
- Débrancher votre smartphone du PC
- Localiser l'application cda20156-artistchoir-bccjdjts, cliquer dessus et autoriser toutes les étapes, ignorer les
barrières de sécurité.
- Lorsque l'application est installée, faire OK au lieu de Ouvrir
- Aller dans Paramètres > Applis et notifications > rechercher et cliquer sur cda20156-artistchoir-bccjdjts
> Autorisations > Stockage > Cliquer sur Autoriser
- Faire autant de fois Retour que nécessaire pour pouvoir faire Ouvrir, cliquer dessus



--------------------------------------
******* Utiliser l'application *******
--------------------------------------

Notre application vous permettra de créer vos propres fiches outils.

Tout d'abord, il vous faudra vous créer un compte.

Sur l'écran d'accueil, choississer "s'inscrire" et entrez les informations demandées : pseudo, email et mot de passe. Notez qu'il vous faudra entrer deux fois votre email et mot de passe et que ce dernier doit faire 6 caractères minimum.
Une fois les champs remplis, validez et, si votre inscription a bien été prise en compte, vous serez redirigé vers la page d'authentification.


Pour s'identifier, entrez votre pseudo ainsi que votre mot de passe. Une fois l'identification réussie, vous arriverez sur votre tableau de bord où s'offrent à vous 3 options : "mon profil", "créer une fiche produit" et "lister mes fiches produits".

En cliquant sur "mon profil", vous pourrez voir votre mail et votre pseudo, et vous avez la possibilité de changer vos informations en cliquant sur "modifier". Les champs à remplir sont les mêmes que lors de l'inscription.

En choisissant "créer une fiche produit", vous serez redirigé vers un formulaire pour créer une fiche. Vous devrez entrer le nom du produit, sa marque, une description et une référence (13 chiffres). Il est également possible d'y associer une photographie en choisissant "prendre une photo". Cela activera l'appareil photo de votre téléphone et vous pourrez prendre un cliché qui servira à illustrer votre fiche.

Enfin, "lister mes fiches produits" affichera une liste des fiches que vous avez créé. Vous pourrez voir leur nom et référence, et aurez la possibilité de les modifier ou supprimer.

Pour vous déconnecter, vous n'avez qu'à sélectionner l'icône de flèche en haut à droite de l'écran.



Vous savez à présent comment utiliser notre application de gestion de produits, n'hésitez pas à soumettre un retour !

